/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stateless;

import entidad.Libro;
import java.math.BigDecimal;
import java.util.Collection;
import javax.ejb.Remote;

/**
 *
 * @author tavo
 */
@Remote
public interface CatalogoSessionBeanRemote
{

    public Libro addLibro(String titulo, String autor, BigDecimal precio);
    
    public void deleteLibro(Integer id);
    
    public Libro updateLibro(Integer id, String titulo, String autor, BigDecimal precio);
    
    public Libro getLibro(Integer id);
    
    public Collection<Libro> getLibrosByTitulo(String titulo);
    
    public Collection<Libro> getLibrosByAutor(String autor);
    
    public Collection<Libro> getLibrosByPrecio(BigDecimal precio);

    public Collection<Libro> getAllLibros();
}
