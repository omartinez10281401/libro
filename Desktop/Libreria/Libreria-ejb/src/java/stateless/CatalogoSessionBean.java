/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stateless;

import entidad.Libro;
import java.math.BigDecimal;
import java.util.Collection;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tavo
 */
@Stateless
public class CatalogoSessionBean implements CatalogoSessionBeanRemote
{

    @PersistenceContext(unitName = "Libreria-ejbPU")
    EntityManager em;
    protected Libro libro;
    protected Collection<Libro> ListaLibros;

    /**
     *
     * @param titulo
     * @param autor
     * @param precio
     * @return 
     */
    @Override
    public Libro addLibro(String titulo, String autor, BigDecimal precio)
    {
        if(libro==null)
        {
       libro = new Libro(titulo, autor, precio);
        em.persist(libro);
        
        
        }
        return libro;
        
    }

    @Override
    public void deleteLibro(Integer id)
    {
        libro = getLibro(id);
        em.remove(libro);
    }

    @Override
    public Libro updateLibro(Integer id, String titulo, String autor, BigDecimal precio)
    {
        libro = getLibro(id);
        libro.setTitulo(titulo);
        libro.setAutor(autor);
        libro.setPrecio(precio);
        em.flush();
        return libro;
    }

    @Override
    public Libro getLibro(Integer id)
    {
        libro = (Libro) em.find(Libro.class, id);
        return libro;
    }

    @Override
    public Collection<Libro> getLibrosByTitulo(String titulo)
    {
        ListaLibros = em.createNamedQuery("Libro.findByTitulo")
                .setParameter("titulo", titulo)
                .getResultList();
        return ListaLibros;
    }

    @Override
    public Collection<Libro> getLibrosByAutor(String autor)
    {
        ListaLibros = em.createNamedQuery("Libro.findByAutor")
                .setParameter("autor", autor)
                .getResultList();
        return ListaLibros;
    }

    @Override
    public Collection<Libro> getLibrosByPrecio(BigDecimal precio)
    {
        ListaLibros = em.createNamedQuery("Libro.findByPrecio")
                .setParameter("precio", precio)
                .getResultList();
        return ListaLibros;
    }

    @Override
    public Collection<Libro> getAllLibros()
    {
        ListaLibros = em.createNamedQuery("Libro.findAll").getResultList();
        return ListaLibros;
    }
}
