<%-- 
    Created on : 8/11/2014, 02:19:36 PM
    Author     : tavo
            
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="entidad.*,stateless.*,java.math.BigDecimal,javax.naming.*,java.util.*"%>
<%!
    private CatalogoSessionBeanRemote librocat = null;
    Integer s1;

    public void jspInit()
    {
        try
        {
            InitialContext context = new InitialContext();
            librocat = (CatalogoSessionBeanRemote) context.lookup(CatalogoSessionBeanRemote.class.getName());
            System.out.println("Cargando el Catalogo Bean: " + librocat);
        } catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public void jspDestroy()
    {
        librocat = null;
    }
%>
<%
    try
    {
        
        s1 = Integer.parseInt(request.getParameter("id"));
        

        if (s1 != null)
        {
            Libro actualizado = librocat.getLibro(s1);
            
            if (librocat.getLibro(s1)!= null) {
                
            
%>
 <jsp:forward page="../views/eliminar.jsp"></jsp:forward>  

<%
            }
            else{
                %>
                    </br></br></br>
                   <h1>No existe ese libro con ese id</h1>
                   
                   <p>
                   <a href="../views/eliminar_index.jsp" target="_parent"><button>Regresar </button></a>
                   <a href="../index.html" target="_parent"><button>Regresar al inicio</button></a>
                   <p>    
    
<%
         
            }
        }//fin del if
    }// fin del try
    catch (Exception e)
    {
        e.printStackTrace();
    }
%>
