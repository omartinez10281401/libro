<%-- 
    Created on : 8/11/2014, 02:19:36 PM
    Author     : tavo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="entidad.*,stateless.*,java.math.BigDecimal,javax.naming.*,java.util.*"%>
<%!
    private CatalogoSessionBeanRemote librocat = null;
    String s1, s2, s3;
    Collection list;

    public void jspInit()
    {
        try
        {
            InitialContext context = new InitialContext();
            librocat = (CatalogoSessionBeanRemote) context.lookup(CatalogoSessionBeanRemote.class.getName());
            System.out.println("Cargando el Catalogo Bean: " + librocat);
        } catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public void jspDestroy()
    {
        librocat = null;
    }
%>
<%
    try
    {
        list = librocat.getAllLibros();
%>
<header>
    <h1>Libros existentes</h1>
</header>
<hr/>
<%
    for (Iterator iter = list.iterator(); iter.hasNext();)
    {
        Libro elemento = (Libro) iter.next();
%>
<br/>
<p> ID: <b><%= elemento.getId()%></b></p>
<p>Titulo: <b><%= elemento.getTitulo()%></b></p>
<p>Autor: <b><%= elemento.getAutor()%></b></p>
<p>Precio: <b><%= elemento.getPrecio()%></b></p>

<%
    }
%>
<p><a href="../index.html" target="_parent"><button>Regresar al inicio</button></a><p>
<%
    }// fin del try
    catch (Exception e)
    {
        e.printStackTrace();
    }
%>
