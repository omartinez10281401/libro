<%-- 
    Created on : 8/11/2014, 02:19:36 PM
    Author     : tavo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="entidad.*,stateless.*,java.math.BigDecimal,javax.naming.*,java.util.*"%>
<%!
    private CatalogoSessionBeanRemote librocat = null;
    String s1, s2, s3, s4;

    public void jspInit()
    {
        try
        {
            InitialContext context = new InitialContext();
            librocat = (CatalogoSessionBeanRemote) context.lookup(CatalogoSessionBeanRemote.class.getName());
            System.out.println("Cargando el Catalogo Bean: " + librocat);
        } catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public void jspDestroy()
    {
        librocat = null;
    }
%>
<%
    try
    {
        s1 = request.getParameter("id");
        s2 = request.getParameter("t1");
        s3 = request.getParameter("aut");
        s4 = request.getParameter("precio");

        if (s1 != null && s2 != null && s3 != null && s4 != null)
        {
            Integer id = Integer.parseInt(s1);
            Double precio = new Double(s4);
            BigDecimal b = new BigDecimal(precio);
            Libro actualizado = librocat.updateLibro(id, s2, s3, b);
            System.out.println("Registro actualizado");
%>
<header>
    <h1>Libro actualizado</h1>
</header>
<hr/>
<p> ID: <b><%= actualizado.getId()%></b></p>
<p>Titulo: <b><%= actualizado.getTitulo()%></b></p>
<p>Autor: <b><%= actualizado.getAutor()%></b></p>
<p>Precio: <b><%= actualizado.getPrecio()%></b></p>
<p><a href="../index.html" target="_parent"><button>Regresar al inicio</button></a><p>
<%
        }//fin del if
    }// fin del try
    catch (Exception e)
    {
        e.printStackTrace();
    }
%>
