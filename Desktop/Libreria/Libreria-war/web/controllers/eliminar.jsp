<%-- 
    Created on : 8/11/2014, 02:19:36 PM
    Author     : tavo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="entidad.*,stateless.*,java.math.BigDecimal,javax.naming.*,java.util.*"%>
<%!
    private CatalogoSessionBeanRemote librocat = null;
    String s1, s2;

    public void jspInit()
    {
        try
        {
            InitialContext context = new InitialContext();
            librocat = (CatalogoSessionBeanRemote) context.lookup(CatalogoSessionBeanRemote.class.getName());
            System.out.println("Cargando el Catalogo Bean: " + librocat);
        } catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public void jspDestroy()
    {
        librocat = null;
    }
%>
<%
    try
    {
        s1 = request.getParameter("id");
        Integer aux = Integer.parseInt(request.getParameter("id"));
        s2 = request.getParameter("sino");

        if (s1 != null && s2 != null)
            
        {
            Libro datos = librocat.getLibro(aux);

           
            if ("si".equals(s2))
            {
                Integer id = Integer.parseInt(s1);
                librocat.deleteLibro(id);
                System.out.println("Registro eliminado");
            }
%>
<jsp:forward page="../controllers/verTodos.jsp"></jsp:forward>  
<%
        }//fin del if de nulos
    }// fin del try
    catch (Exception e)
    {
        e.printStackTrace();
    }
%>
