<%-- 
    Created on : 8/11/2014, 02:19:36 PM
    Author     : tavo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="entidad.*,stateless.*,java.math.BigDecimal,javax.naming.*,java.util.*"%>
<%!
    private CatalogoSessionBeanRemote librocat = null;
    String s1, s2, s3;

    public void jspInit()
    {
        try
        {
            InitialContext context = new InitialContext();
            librocat = (CatalogoSessionBeanRemote) context.lookup(CatalogoSessionBeanRemote.class.getName());
            System.out.println("Cargando el Catalogo Bean: " + librocat);
        } catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public void jspDestroy()
    {
        librocat = null;
    }
%>
<%
    try
    {
        s1 = request.getParameter("t1");
        s2 = request.getParameter("aut");
        s3 = request.getParameter("precio");

        if (s1 != null && s2 != null && s3 != null)
        {
            Double precio = new Double(s3);
            BigDecimal b = new BigDecimal(precio);
            Libro agregado = librocat.addLibro(s1, s2, b);
            System.out.println("Registro añadido");
%>
<header>
    <h1>Libro registrado</h1>
</header>
<hr/>
<p> ID: <b><%= agregado.getId()%></b></p>
<p>Titulo: <b><%= agregado.getTitulo()%></b></p>
<p>Autor: <b><%= agregado.getAutor()%></b></p>
<p>Precio: <b><%= agregado.getPrecio()%></b></p>
<p><a href="../index.html" target="_parent"><button>Regresar al inicio</button></a><p>
<%
        }//fin del if
    }// fin del try
    catch (Exception e)
    {
        e.printStackTrace();
    }
%>
