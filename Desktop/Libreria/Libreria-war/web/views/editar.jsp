<%-- 
    Created on : 8/11/2014, 02:11:38 PM
    Author     : tavo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="entidad.*,stateless.*,java.math.BigDecimal,javax.naming.*,java.util.*"%>
<%!
    private CatalogoSessionBeanRemote librocat = null;
    Libro consultado;

    public void jspInit()
    {
        try
        {
            InitialContext context = new InitialContext();
            librocat = (CatalogoSessionBeanRemote) context.lookup(CatalogoSessionBeanRemote.class.getName());
            System.out.println("Cargando el Catalogo Bean: " + librocat);
        } catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public void jspDestroy()
    {
        librocat = null;
    }
%>
<%
    try
    {
        Integer id = Integer.parseInt(request.getParameter("id"));
        consultado = librocat.getLibro(id);
%>
<%
    } catch (Exception ex)
    {
        ex.printStackTrace();
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Editar libro</title>
    </head>
    <body>
        <h1>Editar libro</h1>
        <form action="../controllers/editar.jsp" method="POST">
            <label>ID: </label><input type="text" name="id" value="<%= consultado.getId()%>" readonly="">
            <br/>
            <label>Titulo: </label><input type="text" name="t1" value="<%= consultado.getTitulo()%>" size="50" />
            <br/>
            <label>Nombre del Autor: </label><input type="text" name="aut" value="<%= consultado.getAutor()%>" size="50" />
            <br/>
            <label>Introduzca el Precio: </label><input type="text" name="precio" value="<%= consultado.getPrecio()%>" />
            <br/>
            <input type="submit"><input type="reset">
        </form>
    </body>
</html>
