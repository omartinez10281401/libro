<%-- 
    Created on : 8/11/2014, 02:11:38 PM
    Author     : tavo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="entidad.*,stateless.*,java.math.BigDecimal,javax.naming.*,java.util.*"%>
<%!
    private CatalogoSessionBeanRemote librocat = null;
    Libro consultado;

    public void jspInit()
    {
        try
        {
            InitialContext context = new InitialContext();
            librocat = (CatalogoSessionBeanRemote) context.lookup(CatalogoSessionBeanRemote.class.getName());
            System.out.println("Cargando el Catalogo Bean: " + librocat);
        } catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public void jspDestroy()
    {
        librocat = null;
    }
%>
<%
    try
    {
        Integer id = Integer.parseInt(request.getParameter("id"));
        consultado = librocat.getLibro(id);
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Eliminar libro</title>
    </head>
    <body>
        <h1>Eliminar libro</h1>
            <p> ID: <b><%= consultado.getId()%></b></p >
            <p>Titulo: <b><%= consultado.getTitulo()%></b></p>
            <p>Autor: <b><%= consultado.getAutor()%></b></p>
            <p>Precio: <b><%= consultado.getPrecio()%></b></p> 
 
<%
    } catch (Exception ex)
    {
        ex.printStackTrace();
    }
%>

        
        <form action="../controllers/eliminar.jsp" method="POST">
            <label>¿Esta seguro de quere eliminar el libro <%= consultado.getId()%>?</label><br/>
            <input type="radio" name="sino" value="si">Si<br/>
            <input type="radio" name="sino" value="no" checked="">No<br/>
            <input type="hidden" name="id" value="<%= consultado.getId()%>"><br/>
            <input type="submit">
        </form>
    </body>
</html>
