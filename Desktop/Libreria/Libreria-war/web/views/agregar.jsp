<%-- 
    Created on : 8/11/2014, 02:11:38 PM
    Author     : tavo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Nuevo libro</title>
    </head>
    <body>
        <h1>Agregar un nuevo libro</h1>
        <form action="../controllers/agregar.jsp" method="POST">
            <label>Titulo: </label><input type="text" name="t1" value="" size="50" />
            <br/>
            <label>Nombre del Autor: </label><input type="text" name="aut" value="" size="50" />
            <br/>
            <label>Introduzca el Precio: </label><input type="number" step="0.01" name="precio" value="0.00" />
            <br/>
            <input type="submit"><input type="reset">
        </form>
    </body>
</html>
